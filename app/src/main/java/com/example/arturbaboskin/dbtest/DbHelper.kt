package com.example.arturbaboskin.dbtest


import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DbHelper(context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {

    companion object {

        private const val DB_NAME = "notes.db"
        private const val DB_VERSION = 1

        const val NOTES_TABLE_NAME = "notes"
        const val NOTES_NAME = "name"
        const val NOTES_DESCRIPTION = "description"

        private val DB_CREATE = StringBuilder()
                .append("CREATE TABLE ")
                .append(NOTES_TABLE_NAME)
                .append(" (id INTEGER PRIMARY KEY AUTOINCREMENT, ")
                .append(NOTES_NAME)
                .append(" VARCHAR(100), ")
                .append(NOTES_DESCRIPTION)
                .append(" TEXT)").toString()
    }

    override fun onCreate(sqLiteDatabase: SQLiteDatabase) = sqLiteDatabase.execSQL(DB_CREATE)

    override fun onUpgrade(sqLiteDatabase: SQLiteDatabase, i: Int, i1: Int) {}
}
