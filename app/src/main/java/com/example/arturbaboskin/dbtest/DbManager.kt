package com.example.arturbaboskin.dbtest


import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import java.util.*

class DbManager private constructor() {

    private var db: SQLiteDatabase? = null

    companion object {

        private var instance: DbManager? = null

        fun getInstance(context: Context): DbManager {
            if (instance == null) {
                instance = DbManager()
                instance!!.dbInit(context)
            }
            return instance!!
        }
    }

    fun getNotes(): ArrayList<Note> {
        val notes = ArrayList<Note>()
        val cursor = db!!.query(DbHelper.NOTES_TABLE_NAME,
                arrayOf("id", DbHelper.NOTES_NAME, DbHelper.NOTES_DESCRIPTION), null, null, null, null, null)

        val columnId = cursor.getColumnIndex("id")
        val columnName = cursor.getColumnIndex(DbHelper.NOTES_NAME)
        val columnDescription = cursor.getColumnIndex(DbHelper.NOTES_DESCRIPTION)

        while (cursor.moveToNext()) {
            val note = Note(cursor.getInt(columnId),
                    cursor.getString(columnName),
                    cursor.getString(columnDescription))
            notes.add(note)
        }
        return notes
    }

    fun addNote(note: Note) {
        val contentValues = ContentValues()
        contentValues.put(DbHelper.NOTES_NAME, note.name)
        contentValues.put(DbHelper.NOTES_DESCRIPTION, note.description)
        db!!.insert(DbHelper.NOTES_TABLE_NAME, null, contentValues)
    }

    private fun dbInit(context: Context) {
        val helper = DbHelper(context)
        db = helper.writableDatabase
    }
}
