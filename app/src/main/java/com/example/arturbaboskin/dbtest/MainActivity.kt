package com.example.arturbaboskin.dbtest

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        for (i in 1..10) DbManager.getInstance(applicationContext)
                .addNote(Note("name_$i", "description"))

        val notes = DbManager.getInstance(applicationContext).getNotes()

        val adapter = ArrayAdapter(applicationContext, android.R.layout.simple_list_item_1, notes)

        list.adapter = adapter
    }
}
