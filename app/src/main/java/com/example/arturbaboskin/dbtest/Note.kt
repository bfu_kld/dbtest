package com.example.arturbaboskin.dbtest

data class Note(var name: String? = null,
                var description: String? = null) {

    var id: Int = 0
        private set

    constructor(id: Int, name: String, description: String) : this(name, description) {
        this.id = id
    }

    override fun toString(): String {
        return id.toString() + " | " + name + " | " + description
    }
}
